package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Mono {

	private double x;
	private double y;
	private double ancho;
	private double alto;
	private Color color;
	private Integer vidas;
	int tiempoSalto;
	static boolean monoEstaSaltando;
	
	private Integer puntaje; 

	
	private Image iconoMono;
	
	public Mono(double x, double y) {
		this.x = x;
		this.y = y;
		this.ancho = 20;
		this.alto = 65;
		this.color = Color.red;
		this.vidas = 3; 
		this.puntaje = 0;
		
		this.iconoMono = Herramientas.cargarImagen("Monkey-01.png");
	}
	
	public void dibujar(Entorno e) {
		e.dibujarRectangulo(x+10, y-7, ancho, alto, 29, color);
		e.dibujarImagen(iconoMono, x, y, 0, 0.14);
	}

	public void inicioSalto() {
		this.tiempoSalto =60;
	}
	
	public void saltar() {
		if(this.tiempoSalto>0) {
		   this.y-=4;
		   this.tiempoSalto--;
		   monoEstaSaltando = true;
		}
	}
	
	

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getAncho() {
		return ancho;
	}

	public double getAlto() {
		return alto;
	}
	
	public double vidasRestantes() {
		return vidas;
	}

	public void tocaEnemigo() {
		vidas--;
	}
	public void tocaObstaculo() {
		puntaje = puntaje-5;
	}
	public void saltoTronco() {
		puntaje++;
	}
	public void monoSumaPuntos() {
		puntaje = puntaje+5;
	}
	public String imprimirPuntaje() {
		return "Score: " + puntaje.toString();
	}
	public int puntajeActualizado() {
		return puntaje;
	}
	public String imprimirVidas() {
		return "Vidas restantes: " + vidas.toString();
	}

	public void caer() {
		y++;
	}

	public boolean estaEnArboles(Arbol[] arboles) {
		for(int i=0; i<arboles.length;i++) {
			if(arboles[i]!=null && arboles[i].chocaConMono(this)) {
				return true;
			}
		}
		return false;
	}

	public boolean estaEnPiso() {
		return y>550;
	}

}
