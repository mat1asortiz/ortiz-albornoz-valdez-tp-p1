package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Serpiente {

	private double x;
	private double y;
	private double ancho;
	private double alto;
	private double velocidad;
	private Image iconoSerpiente;
	
	public Serpiente(double x, double y) {
		this.x = x;
		this.y = y;
		this.alto = 40;
		this.ancho = 40;
		this.velocidad = 2;
		this.iconoSerpiente = Herramientas.cargarImagen("serpiente03.png");
	}
	
	public static void agregarSerpiente(Serpiente[] serpientes, Arbol a) {
		for (int i=0 ; i<serpientes.length; i++) {
				if(serpientes[i] == null){
					serpientes[i] = new Serpiente(875,450-a.valorDiametro());
					return;
			}
		}
	}
	
	public void dibujar(Entorno e) {
		e.dibujarImagen(iconoSerpiente, x, y, 0, 0.40);
	}

	public void avanzar() {
		this.x -= velocidad;
	}

	// esto no haria falta creo.
	public double diametroValor(Arbol a) {
		return a.valorDiametro();
	}
	
	public boolean chocaConEntorno(Entorno e) {
		if (x < -70) {
			return true;
		}
		return false;
	}
	
	public boolean chocaConMono(Mono m) {
		return (m.getX() - m.getAncho()/2 < x+25 + ancho/2 && 
				x-25 - ancho/2 < m.getX() + m.getAncho()/2 &&
				
				m.getY() - m.getAlto()/2 < y+10 + ancho/2 &&
				y-10 - alto/2 < m.getY() + m.getAlto()/2);
	}
	
	public boolean chocaConPiedra(Piedra p) {
		return (p.getX() - p.getTamanio()/2 < x + ancho/2 && 
				x - ancho/2 < p.getX() + p.getTamanio()/2 &&
				
				p.getY() - p.getTamanio()/2 < y + alto/2 &&
				y - alto/2 < p.getY() + p.getTamanio()/2);
	}
	
	public double serpienteEstaEnArbol(Arbol a) {
		return a.getY();
	}
	

}
