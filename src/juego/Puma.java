package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Puma {

	private double x;
	private double y;
	private double ancho;
	private double alto;
	private Color color;
	private double velocidad;
	private Image iconoPuma;
	
	public Puma(double x, double y) {
		this.x = x;
		this.y = y;
		this.alto = 30;
		this.ancho = 80;
		this.color = Color.yellow;
		this.velocidad = 3;
		this.iconoPuma = Herramientas.cargarImagen("puma01.png");
	}
	
	public static void agregarPuma(Puma[] pumas) {
		for (int i=0 ; i<pumas.length; i++) {
			if(pumas[i] ==null) {
				pumas[i] = new Puma(870,550);
				return;
			}
		}
	}
	
	public void dibujar(Entorno e) {
		e.dibujarRectangulo(x, y-15, ancho, alto, 0, color);
		e.dibujarImagen(iconoPuma, x, y, 0.2, 0.25);
	}

	public void avanzar() {
		this.x -= velocidad;
	}
	
	public boolean chocaConEntorno(Entorno e) {
		if (x < -70) {
			return true;
		}
		return false;
	}
	
	public boolean chocaConMono(Mono m) {
		return (m.getX() - m.getAncho()/2 < x + ancho/2 && 
				x - ancho/2 < m.getX() + m.getAncho()+50/2 &&
				
				m.getY() - m.getAlto()/2 < y + ancho/2 &&
				y - alto/2 < m.getY()+15 + m.getAlto()/2);
	}
	
	public boolean chocaConPiedra(Piedra p) {
		return (p.getX() - p.getTamanio()/2 < x + ancho/2 && 
				x - ancho/2 < p.getX() + p.getTamanio()/2 &&
				
				p.getY() - p.getTamanio()/2 < y + alto/2 &&
				y - alto/2 < p.getY() + p.getTamanio()/2);
	}

}
