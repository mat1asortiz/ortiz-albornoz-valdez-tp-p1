package juego;

import java.awt.Color;

import entorno.Entorno;

public class Piedra {
	private double x;
	private double y;
	private double velocidad;
	private double tamanio;
	private Color color;

	public Piedra(double x, double y) {
		this.x = x;
		this.y = y;
		this.velocidad = 4;
		this.tamanio = 40;
		this.color = Color.GRAY;

	}
	
	public static void agregarPiedra(Piedra[] piedras,Mono m) {
		for (int i=0 ; i<piedras.length; i++) {
			if(piedras[i] == null) {
				piedras[i] = new Piedra(m.getX(),m.getY());
				return;
			}
		}
	}
	
	public void dibujar(Entorno entorno) {
		entorno.dibujarCirculo(x, y, tamanio, color);
	}

	public void avanzar() {
		this.x += velocidad;
	}
	
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
	
	public double getTamanio() {
		return tamanio;
	}

	public boolean chocaConEntorno(Entorno e) {
		if (x > 870) {
			return true;
		}
		return false;
	}

	public boolean chocaConPuma(Puma[] pumas) {
		for(int i=0; i<pumas.length;i++) {
			if(pumas[i]!=null && pumas[i].chocaConPiedra(this)) {
				pumas[i] = null;
				return true;
			}
		}
		return false;
	}
	
	public boolean chocaConSerpiente(Serpiente[] serpientes) {
		for(int i=0; i<serpientes.length;i++) {
			if(serpientes[i]!=null && serpientes[i].chocaConPiedra(this)) {
				serpientes[i] = null;
				return true;
			}
		}
		return false;
	}
	
}
