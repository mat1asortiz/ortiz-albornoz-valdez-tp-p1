package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Arbol {

	private double x;
	private double y;
	private double ancho;
	private double alto;
	private Color color;
	private double velocidad;
	private Image iconoArbol;
	private double diametro;
	static boolean serpientePuedeAparecer;
	
	public Arbol(double x, double y) {
		this.diametro = Math.random();
		
		this.x = x;
		this.y = y;
		this.ancho = 150;
		this.alto = 40;
		this.color = Color.black;
		

		this.velocidad = 2;
		this.iconoArbol = Herramientas.cargarImagen("arbol02.png");
	}
	
	public static void agregarArbol(Arbol[] arboles) {
		for (int i=0 ; i<arboles.length; i++) {
			if(arboles[i] == null) {
				arboles[i] = new Arbol(875,500);
				return;
			}
		}
	}
	
	public void dibujar(Entorno e) {
		e.dibujarImagen(iconoArbol, x, y-(diametro*100), 0, 0.6+diametro);
		e.dibujarRectangulo(x, y-(diametro*100), ancho, alto, 0, color);
	}
	
	public void avanzar() {
		this.x -= velocidad;
	}
	
	public boolean chocaConEntorno(Entorno e) {
		if (x < -75) {
			return true;
		}
		return false;
	}
	
	public double getX() {
		return x;
	}
	

	public double getY() {
		return y;
	}
	
	public double getAncho() {
		return ancho;
	}

	public double getAlto() {
		return alto;
	}
	public double valorDiametro() {
		return diametro*100; 
	}
	


	
	public boolean chocaConMono(Mono m) {
		return (m.getX() - m.getAncho()/2 < x+10 + ancho/2 && 
				x+10 - ancho/2 < m.getX() + m.getAncho()/2 &&
				
				m.getY() + m.getAlto()/2< y-7-(diametro*100)+alto/2 && m.getY()+m.getAlto()/2> y-7-(diametro*100)-alto/2);
	}
	
	public double arbolChocaConSerpiente(Serpiente[] serpientes) {
				return getY();
	}

}