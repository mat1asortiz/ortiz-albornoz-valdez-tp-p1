package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {

	private Entorno entorno;
	private Puma[] pumas;
	private Arbol[] arboles;
	private Serpiente[] serpientes;
	private Piedra[] piedras;
	private Obstaculo[] obstaculos;
	private Mono mono;
	private double tiempoDeJuego;
	private boolean puedeBajar;
	private Image fondo;
	private double tiempoEnArbol;
	
	public Juego() {
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Titulo de TP - Grupo 7 -Ortiz -Albornoz -Valdez - V0.01", 800, 600);
		
		// Inicializar lo que haga falta para el juego
		this.pumas= new Puma[4];
		
		this.arboles = new Arbol[3];
		
		this.serpientes = new Serpiente[1];
		
		this.piedras = new Piedra[1];
		
		this.obstaculos = new Obstaculo[1];
		
		this.mono = new Mono(120,550);
		this.fondo = Herramientas.cargarImagen("image0.png");
		
		this.tiempoDeJuego = 0;
		
		this.puedeBajar = true;
		this.tiempoEnArbol = 0;
		

		// Inicia el juego!
		this.entorno.iniciar();
	}
	public void tick() {
		// Procesamiento de un instante de tiempo
		
		entorno.dibujarImagen(fondo, entorno.ancho() / 2, entorno.alto() / 2, 0, 1);
		if (mono.vidasRestantes() <= 0) {
			entorno.cambiarFont("Arial", 100, Color.RED);
			entorno.escribirTexto("GAME OVER", entorno.ancho() * 0.15, entorno.alto() / 2);
			return;
		}
		
		if (mono.puntajeActualizado()==200) {
			entorno.cambiarFont("Arial", 100, Color.RED);
			entorno.escribirTexto("GANASTE!", entorno.ancho() * 0.15, entorno.alto() / 2);
			entorno.cambiarFont("Arial", 60, Color.RED);
			entorno.escribirTexto("Llegaste a 200 puntos", entorno.ancho() * 0.10, entorno.alto() / 4);
			return;
		}
		
		entorno.escribirTexto(mono.imprimirPuntaje(), entorno.ancho() * 0.05, entorno.alto() * 0.05);
		entorno.escribirTexto(mono.imprimirVidas(), entorno.ancho() * 0.8, entorno.alto() * 0.05);
		
		if (tiempoDeJuego%220 ==0) {
			Arbol.agregarArbol(this.arboles);
//			Serpiente.agregarSerpiente(this.serpientes,this.arboles);
//			Puma.agregarPuma(this.pumas);
			Obstaculo.agregarObstaculos(this.obstaculos);
		}
		
		if(mono.puntajeActualizado()>100 && mono.puntajeActualizado()<150) {
			if(tiempoDeJuego%1000 ==0) {
				Obstaculo.agregarObstaculos(this.obstaculos);
			}
		}if(mono.puntajeActualizado()>150) {
			if(tiempoDeJuego%500 ==0) {
				Obstaculo.agregarObstaculos(this.obstaculos);
			}
		}
	
			// ARBOLES
			for (int i = 0; i < arboles.length; i++) {
				if (arboles[i] != null) {
					arboles[i].dibujar(entorno);
					arboles[i].avanzar();
					if(arboles[i].chocaConEntorno(entorno)){
						arboles[i]=null;
					}
				}
			}
			
			
//			// SERPIENTES
			for (int i = 0; i < serpientes.length; i++) {
				if (serpientes[i] != null) {
					serpientes[i].dibujar(entorno);
					serpientes[i].avanzar();
					if(serpientes[i].chocaConMono(mono)) {
						serpientes[i]=null;
						mono.tocaEnemigo();
					}
					else if(serpientes[i].chocaConEntorno(entorno)){
						serpientes[i]=null;
					}
				}
			}
		
			
			// PUMAS
			for (int i = 0; i < pumas.length; i++) {
				if (pumas[i] != null) {
					pumas[i].dibujar(entorno);
					pumas[i].avanzar();
					if(pumas[i].chocaConMono(mono)) {
						pumas[i]=null;
						mono.tocaEnemigo();
					}
					else if(pumas[i].chocaConEntorno(entorno)){
						pumas[i]=null;
					}
				}
			}
			
			
			// OBSTACULOS
			for (int i = 0; i < obstaculos.length; i++) {
				if (obstaculos[i] != null) {
					obstaculos[i].dibujar(entorno);
					obstaculos[i].avanzar();
					if(obstaculos[i].chocaConMono(mono)) {
						obstaculos[i]=null;
						mono.tocaObstaculo();
					}
					else if(obstaculos[i].chocaConEntorno(entorno)){
						obstaculos[i]=null;
					}
				}
			}
			
			// PIEDRAS
			for (int i = 0; i < piedras.length; i++) {
				if (piedras[i] != null) {
					piedras[i].dibujar(entorno);
					piedras[i].avanzar();
				}
					if(piedras[i] != null && (piedras[i].chocaConPuma(pumas) || piedras[i].chocaConSerpiente(serpientes))){
						piedras[i]=null;
						mono.monoSumaPuntos();
					} 
					if(piedras[i] != null && piedras[i].chocaConEntorno(entorno)){
						piedras[i]=null;
					}
				}
		
			
			
			// movimientos del pj	
			if (mono.estaEnArboles(arboles)) {
				puedeBajar = false;
				tiempoEnArbol++;
				if (tiempoEnArbol<2 && Mono.monoEstaSaltando==false){
					mono.monoSumaPuntos();
				}
			} else {
				puedeBajar = true;
				tiempoEnArbol =0;
			}
			
			if(!mono.estaEnPiso() && puedeBajar) {
				mono.caer();
				Mono.monoEstaSaltando = false;
			}
			
			if(entorno.sePresiono(entorno.TECLA_ARRIBA) && (mono.estaEnPiso() || mono.estaEnArboles(arboles))) {
				mono.inicioSalto();
			}
			
			mono.saltar();
			
			if (entorno.estaPresionada(entorno.TECLA_ESPACIO)) {
				Piedra.agregarPiedra(this.piedras,this.mono);
			}
			
			// agregando los arboles y enemigos.
			 
			
			
//			Random r = new Random();
//			int segundos = r.nextInt(11) * 50;
//			
//			if (tiempoDeJuego % segundos == 0) {
//				agregar arbol.
//			}
//		
			tiempoDeJuego++;
		
//		tiempoDisparo++;
//		if(tiempoDisparo >= 20) {
//			puedeDisparar = true;
//		}
		
		

		mono.dibujar(entorno);
		
	}
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}

}