package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Obstaculo {
	private double x;
	private double y;
	private double ancho;
	private double alto;
	private double velocidad;
	private Image iconoBasura;

	public Obstaculo(double x, double y) {
		this.x = x;
		this.y = y;
		this.alto = 65;
		this.ancho = 40;
		this.velocidad = 4;
		this.iconoBasura = Herramientas.cargarImagen("basura.png");
	}
	
	public static void agregarObstaculos(Obstaculo[] obstaculos) {
		for (int i=0 ; i<obstaculos.length; i++) {
			if(obstaculos[i] == null) {
				obstaculos[i] = new Obstaculo(800,555);
				return;
			}
		}
	}
	
	public void dibujar(Entorno e) {
		e.dibujarImagen(iconoBasura, x, y, 0, 0.15);
	}

	public void avanzar() {
		this.x -= velocidad;
	}
	
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}
	

	public boolean chocaConEntorno(Entorno e) {
		if (x < -70) {
			return true;
		}
		return false;
	}

	public boolean chocaConMono(Mono m) {
		return (m.getX() - m.getAncho()/2 < x + ancho/2 && 
				x - ancho/2 < m.getX() + m.getAncho()+50/2 &&
				
				m.getY() - m.getAlto()/2 < y + ancho/2 &&
				y - alto/2 < m.getY()+15 + m.getAlto()/2);
	}
	
}